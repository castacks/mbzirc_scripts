#!/usr/bin/env sh

# Global parameters
rosparam set /mbzirc/debug 0
rosparam set /mbzirc/log 0
rosparam set /mbzirc/debug 0

# Decktrack parameters
rosparam set /mbzirc/decktrack/log 0
rosparam set /mbzirc/decktrack/debug 0
rosparam set /mbzirc/decktrack/calib 0
rosparam set /mbzirc/decktrack/calib_input ""
rosparam set /mbzirc/decktrack/calib_output ""

