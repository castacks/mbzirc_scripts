# Overview #

This repository contains scripts for MBZIRC project that are independent of other sub-systems.

### Who do I talk to? ###

* Azarakhsh Keipour (keipour@gmail.com)
* Guilherme Pereira (gpereira@ufmg.br)

# bgit.bash Script #

This script is for working with git repositories in batch. The scripts accepts any git command and performs the command on all git repositories one-by-one. For example, to see the status (changes) of all repositories, the following command can be used from the directory containing the script:

```
#!bash

./bgit.bash status
```

The script has the following assumptions:

* The project's main directory is *~/mbzirc_ws/*
* It performs passed git commands only on repositories with names containing *mbzirc_*

Alter the value of *$GITREPO* variable in the code to change the default project directory and modify the *grep* command in the code to change the default pattern for repository listing.

To simplify the script execution, add a symbolic link to the start-up script (*~/.bashrc* or *~/.profile*):

```
#!bash

echo 'alias bgit=~/mbzirc_ws/src/mbzirc_scripts/bgit.bash' >> ~/.bashrc

```

After opening a new terminal, it is possible to perform any git command on the MBZIRC project repositories from any directory with **bgit** command. For example, For example, to see the status (changes) of the repositories, the following command can be used from any directory (not just the directory containing the script):

```
#!bash

bgit status
```

# Some points to simplify life! #

## SSH Commands ##

To setup shortcuts for ssh do this:

```
#!bash

echo 'alias mbzssh=~/mbzirc_ws/src/mbzirc_scripts/mbzssh.bash' >> ~/.bashrc
echo 'alias mbzinit=~/mbzirc_ws/src/mbzirc_scripts/mbzinit.bash' >> ~/.bashrc

```

Now, in order to ssh to the manifold, you can just type *mbzssh* for wifi connection or "mbzssh -l" for lan connection.

The defined *mbzinit* command will open three tabs, updates the date of the manifold and does ssh in all the three tabs. For wifi connection, you can just type *mbzinit* and for lan connection you can type "mbzinit -l".

## Viewing all the files on manifold in "Files" ##

In order to see all the files on manifold in folder view, press Ctrl+L keys in "Files" program. Then enter this location:

```
#!bash

sftp://ubuntu@192.168.0.170
```

Note that for lan connection, the IP address would be 192.168.1.170. 

In order to see the home directory of manifold, go to *home/ubuntu/* directory.

## Eliminate the need for password during SSH ##

To eliminate the need for entering password each time during ssh, do this on your computer:

*If you are connected through Wifi:*

```
ssh-keygen -t rsa
cat .ssh/id_rsa.pub | ssh ubuntu@192.168.0.170 'cat >> .ssh/authorized_keys'
cat .ssh/id_rsa.pub | ssh root@192.168.0.170 'cat >> .ssh/authorized_keys'
```

*If you are connected through Lan:*

```
ssh-keygen -t rsa
cat .ssh/id_rsa.pub | ssh ubuntu@192.168.1.170 'cat >> .ssh/authorized_keys'
cat .ssh/id_rsa.pub | ssh root@192.168.1.170 'cat >> .ssh/authorized_keys'
```

In case you get an error to eliminate the password for root user (perhaps it is the first time someone tries to ssh using root user), then follow these steps:

- SSH using **ubuntu** user

- Switch to super user (using `su` command)

- Run `passwd` and enter the old and new passwords for the root user

- Open `/etc/ssh/sshd_config` using admin privileges

- Find `PermitRootLogin without-password` line and change it to `PermitRootLogin yes`

- Restart the ssh service (`sudo service ssh restart`) or reboot