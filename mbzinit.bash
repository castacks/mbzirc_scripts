#!/bin/bash

tab="--tab-with-profile=Default"
wdir="--working-directory=/home/mbzirc/mbzirc_ws/src/mbzirc_scripts/"
cmd="./mbzssh.bash $1"
cmddate="./mbzdate.bash $1"

gnome-terminal $tab $wdir --title=Date -e "$cmddate" $tab $wdir --title=Camera -e "$cmd" $tab $wdir --title=Mission -e "$cmd" $tab $wdir --active --title=Tab1 -e "$cmd"
