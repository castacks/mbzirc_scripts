#!/bin/bash
cwd=$(pwd)

if [ ! "$1" = "" ] ; then

   if [ "$GITREPO" = "" -a -d "$HOME/mbzirc_ws/src" ] ; then
      GITREPO="$HOME/mbzirc_ws/src"
   fi

   if [ "$GITREPO" != "" ] ; then

      DIRS="`/bin/ls -1 $GITREPO | grep 'mbzirc_'`"

      for dir in $DIRS ; do

         if [ -d $GITREPO/$dir/.git ] ; then
            echo "$(tput setaf 6)$(tput bold)$dir$(tput sgr 0) -> git $@"
            cd $GITREPO/$dir ; git $@
            echo
         fi

      done
   else

      echo "Git repositories not found."

   fi
fi

cd $cwd
